"use strict";

function Fruta() {
  var nombre;
  var familia;
  this.setNombre = function(value) {
    nombre = value;
  }
  this.getNombre = function() {
    return nombre;
  }
  // queremos que sea un metodo privado
  this.setFamilia = function(value) {
    familia = value;

  }

  return {
    setNombre: this.setNombre,
    getNombre: this.getNombre
  }

}

var limon = new Fruta();

console.log(limon);

limon.setNombre('Citrus');

console.log(limon.getNombre());
//console.log(limon.setFamilia());