"use strict";

function creaAgente(nombre) {
  let edad = 0;
  return { // este objeto es el agente que estoy creando
    setNombre: function(nuevoNombre) {
      nombre = nuevoNombre;
    },
    getNombre: function() { return nombre; },
    setEdad: function(nuevaEdad) {
      edad = nuevaEdad;
    },
    getEdad: function() { return edad; },
    saluda: function() {
      console.log('Hola soy', nombre);
    }
  }
}

const smith = creaAgente('Smith');
smith.setEdad(33);

const jones = creaAgente('Jones');
jones.setEdad(44);

console.log('smith:', smith.getNombre(), smith.getEdad());
console.log('jones:', jones.getNombre(), jones.getEdad());

setTimeout(smith.saluda, 1000);
