"use strict";

const versionModulo = require('./versionModulo');
const fs = require('fs');
const path = require('path');
const async = require('async');

// recorrer directorios de ./node_modules y sacar versiones de los modulos

function versionModulos(callBack) {

  const ruta_modulos = './node_modules';

  fs.readdir(ruta_modulos, function(err, lista) {

    async.concat(lista, function iterador(modulo, callbackSiguiente) {

      if (modulo[0] === '.') {
        callbackSiguiente();
        return;
      }

      versionModulo(modulo, function(err, version) {
        if (err) {
          callbackSiguiente(err);
          return;
        }

        callbackSiguiente(null, {modulo: modulo, version: version});

      });

    }, function finalizador(err, modulos) {

      callBack(err, modulos);

    });


  });

}

versionModulos(function(err, resultado) {
  console.log('terminado', err, resultado);
});