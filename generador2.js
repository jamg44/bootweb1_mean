"use strict";

function espera(milis, n, cb) {
  setTimeout(() => {
    cb(null, {id: n, name: 'Brown'});
  }, milis);
}

run( function* (sigue) {

  for( let i = 0; i<5; i++) {
    let res = yield espera(1000, i, sigue);
    console.log('res', res);
  }


});



function run(generador) {
  const iterador = generador(sigue);
  function sigue() {
    const argumentos = Array.prototype.slice.call(arguments);
    iterador.next(argumentos);
  }
  iterador.next();
}