"use strict";

const request = require('request');

// hacemos una petición a StarWars API

request({url:'https://swapi.co/api/people/1', json: true}, function(err, resp, body) {
  if (err || resp.statusCode >= 400) {
    return console.log('Error', resp.statusCode, err);
  }

  console.log(body, 'es del planeta:');
  request({url: body.homeworld, json: true}, function(err, resp, body) {
    if (err || resp.statusCode >= 400) {
      return console.log('Error', resp.statusCode, err);
    }

    console.log(body.name);
  });

});