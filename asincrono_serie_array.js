"use strict";

console.log('empiezo');

function escribeTras2Segundos(el, callBack) {
  setTimeout(function() {
    console.log('texto' + el);
    callBack();
  }, 2000);
}

// llama a una función n veces en serie,
// al finalizar llame a un callBack de finalización
function serieArray(arr, fn, callBackFin) {
  if (arr.length == 0) {
    callBackFin();
    return;
  }
  fn(arr.shift(), function() {
    serieArray(arr, fn, callBackFin);
  })
}

serieArray([1, 2, 3, 4, 5], escribeTras2Segundos, function() {
  console.log('he terminado');
});
