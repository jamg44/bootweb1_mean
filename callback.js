"use strict";

function suma(a, b, callBack) {
  let resultado = a + b;
  callBack(resultado);
}

suma(10, 5, function(res) {
  console.log(res);
});
