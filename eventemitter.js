"use strict";

const events = require('events');

const eventEmitter = new events.EventEmitter();

const suenaTelefono = function(quien) {
  if (quien !== 'madre') {
    console.log('ring ring');
  }
};

const vibrarTelefono = function(quien) {
  console.log('brr brr');
};

eventEmitter.on('llamar telefono', suenaTelefono);
eventEmitter.on('llamar telefono', vibrarTelefono);

eventEmitter.emit('llamar telefono', 'madre');