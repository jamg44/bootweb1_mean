"use strict";

const MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/cursonode', function(err, db) {
  if (err) {
    return console.log(err);
  }
  db.collection('agentes').find().toArray(function(err, docs) {
    if (err) {
      return console.log(err);
    }
    console.log(docs);
    db.close();
  });
});