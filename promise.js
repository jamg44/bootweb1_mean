"use strict";
// Hacer una promesa que espara N milisegundos

function sleep(milis) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      //if (err) {
        //return reject(new Error('Todo va fatal!'));
      //}

      resolve(5 + 6);
    }, milis);
  });
}

const promesa = sleep(0);

console.log(promesa);

// promesa termina bien
promesa
  .then(function(resultado) {
    console.log(resultado);
  })
  .catch(function(err) {
    console.log('Ha habido un error:', err);
  });
