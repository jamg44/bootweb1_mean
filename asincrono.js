"use strict";

console.log('empiezo');

function escribeTras2Segundos(texto, callBack) {
  setTimeout(function() {
    console.log(texto);
    callBack();
  }, 2000);
}

escribeTras2Segundos('texto1', function() {
  console.log('texto escrito1');
  escribeTras2Segundos('texto2', function() {
    console.log('texto escrito2');
  });
});


console.log('acabo');