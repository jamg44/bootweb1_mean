
// cargar libreria http
const http = require('http');

// crear el servidor
const server = http.createServer(function(request, response) {

  response.writeHead(200, {'Content-Type': 'text/html; charset=UTF-8'});

  response.end('Wake up, <b>Neo</b>...\n');

});

// arrancarlo
server.listen(1337, '127.0.0.1');
console.log('Servidor arrancado!');
