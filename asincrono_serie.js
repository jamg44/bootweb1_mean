"use strict";

console.log('empiezo');

function escribeTras2Segundos(el, callBack) {
  setTimeout(function() {
    console.log('texto' + el);
    callBack();
  }, 2000);
}

// llama a una función n veces en serie,
// al finalizar llame a un callBack de finalización
function serie(n, fn, callBackFin) {
  if (n <= 0) {
    callBackFin();
    return;
  }
  n--;
  fn(n, function() {
    serie(n, fn, callBackFin);
  })
}

serie(5, escribeTras2Segundos, function() {
  console.log('he terminado');
});
