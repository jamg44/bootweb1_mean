"use strict";

// creamos un constructor de objetos Persona

const Persona = function(name) {
  this.name = name;
};

const pepe = new Persona('Pepe');


// poner en su cadena de prototipos un método nuevo

Persona.prototype.saluda = function() {
  console.log('Hola, me llamo', this.name);
};

pepe.saluda();

// ------------ herencia simple
// heredamos el constructor
function Agente(name) {
  Persona.call(this, name); // super();
}

// le asignamos los métodos de los prototipos
Agente.prototype = new Persona('soy un prototipo!');

const agente = new Agente('Smith');

agente.saluda();

console.log(
  Object.getPrototypeOf(agente),
  agente instanceof Agente,
  agente instanceof Persona,
  agente instanceof Object
);


// ------------ herencia múltiple

function mixinSuperHeroe() {
  this.vuela = function() {
    console.log(this.name, 'vuela');
  };
  this.esquivaBalas = function () {
    console.log(this.name, 'esquiva balas');
  }
}

// si queremos aplicarlo a todos los agentes
Object.assign(Agente.prototype, new mixinSuperHeroe());

agente.vuela();
agente.esquivaBalas();

console.log('-------');
for (let prop in pepe) {
  if (pepe.hasOwnProperty(prop)) {
    console.log('prop', prop);
  }
}