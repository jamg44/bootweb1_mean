"use strict";

function* creaIterador(start, end) {
  while(start < end) {
    yield start;
    start += 1;
  }
  return 'fin';
}

const iterador = creaIterador(0, 5);

// console.log(iterador.next());
// console.log(iterador.next());
// console.log(iterador.next());
// console.log(iterador.next());
// console.log(iterador.next());
// console.log(iterador.next());
//
// console.log(iterador.next());

for (let valor of iterador) {
  console.log(valor);
}