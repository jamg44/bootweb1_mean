"use strict";

var express = require('express');
var router = express.Router();


router.get('/', function(req, res, next) {
  console.log('peticion!');
  //res.send('ok tio');
  next();
});

/* GET home page. */
router.get('/', function(req, res, next) {

  const segundo = (new Date()).getSeconds();

  res.render('index', {
    title: 'Express',
    parrafo: '<p>EJS lo escapa</p>',
    sinEscapar: '<i>sin escapar</i>',
    condicion: {
      segundo: segundo,
      estado: segundo % 2 === 0
    },
    users: [{name: 'Maria'}, {name: 'Ana'}, {name: 'Pilar'}],
    color: 'red'
  });
});

router.get('/ruta/:numero([0-5]+)/piso/:piso(A|B|C)', function(req, res, next) {
  console.log('req.params', req.params);
  res.send('he recibido ' + req.params.numero);
});

router.get('/query', function(req, res, next) {
  console.log('req.query', req.query);
  res.send('he recibido ' + JSON.stringify(req.query));
});

router.post('/post', function(req, res, next) {
  console.log('req.body', req.body);
  res.send('he recibido ' + JSON.stringify(req.body));
});


module.exports = router;
