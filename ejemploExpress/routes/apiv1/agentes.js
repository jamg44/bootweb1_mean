"use strict";

const express = require('express');
const router = express.Router();

const mongoose = require('mongoose');
const Agente = mongoose.model('Agente');

const basicAuth = require('../../lib/basicAuth');
//router.use(basicAuth('admin', '1234'));

const jwtAuth = require('../../lib/jwtAuth');

router.use(jwtAuth());

// recuperar lista de agentes
router.get('/', function(req, res, next) {

  const name = req.query.name;
  const age = req.query.age;
  const limit = parseInt(req.query.limit) || 0;
  const skip = parseInt(req.query.skip) || 0;
  const fields = req.query.fields || null;
  const sort = req.query.sort;

  // creo un filtro vacio
  const filter = {};

  if (name) {
    filter.name = name;
  }

  if (age) {
    filter.age = age;
  }

  Agente.list(filter, limit, skip, fields, sort, function(err, docs) {
    if (err) {
      next(err);
      return;
    }
    res.json({success: true, data: docs});
  });
});

// crear un agente
router.post('/', function(req, res, next) {
  const agente = new Agente(req.body);

  // cambio de password simulado: (ejemplo de metodos de instancia)
  agente.changePassword('1234', function() {});

  agente.save(function(err, agenteCreado) {
    if (err) {
      next(err);
      return;
    }
    res.json({success: true, data: agenteCreado});
  });

});

// actualizar un agente
router.put('/:id', function(req, res, next) {
  const id = req.params.id;
  const agente = req.body;
  Agente.update({_id: id}, agente, function(err) {
    if (err) {
      return next(err);
    }
    res.json({success: true});
  });

});

// eliminar un agente
router.delete('/:id', function(req, res, next) {
  const id = req.params.id;
  Agente.remove({_id: id}, function(err) {
    if (err) {
      return next(err);
    }
    res.json({success: true});
  })
});

module.exports = router;
