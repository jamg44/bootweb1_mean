"use strict";

const express = require('express');
const router = express.Router();

const jwt = require('jsonwebtoken');
const localConfig = require('../../localConfig');

router.post('/authenticate', function(req, res, next) {

  const userName = req.body.username;
  const password = req.body.password;

  // Buscamos en la base de datos un usuario con userName

  // User.find({username: userName})...

  const user = { _id: '33', name: 'Smith' }; // usuario simulado existente en BD

  // y comprobamos su password...

  // si coincide, creamos token

  // no meter el objeto user entero en el token!!!!!!!!!
  const token = jwt.sign({_id: user._id}, localConfig.jwt.secret, {
    expiresIn: localConfig.jwt.expiresIn
  });

  res.json({success: true, token: token});

});

module.exports = router;