"use strict";

const mongoose = require('mongoose');

// primero creamos un esquema

const agenteSchema = mongoose.Schema({
  name: String,
  age: Number
});

// ponemos un método al schema
agenteSchema.statics.list = function(filter, limit, skip, fields, sort, cb) {
  const query = Agente.find(filter);
  query.limit(limit);
  query.skip(skip);
  query.select(fields);
  query.sort(sort);
  query.exec(cb);
};

// metodo de instancia: lo tendrán los objetos de agente (no el modelo)
agenteSchema.methods.changePassword = function(newPasword, cb) {
  console.log('cambio la password del agente', this.name);
  cb();
};

// y luego creo el modelo

const Agente = mongoose.model('Agente', agenteSchema);

// no me hace falta exportalo porque mongoose me lo guarda internamente
//module.exports = Agente;