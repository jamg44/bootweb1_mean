"use strict";

const socketio = require('socket.io');

// Chuleta
// http://stackoverflow.com/questions/10058226/send-response-to-all-clients-except-sender-socket-io

function chatServer(appServer) {
  const io = socketio(appServer);
  let numUsers = 0;


  io.on('connection', function(socket) {

    let addedUser = false;

    socket.on('add user', function(username) {
      socket.username = username;
      numUsers++;
      addedUser = true;
      socket.emit('login', {numUsers: numUsers});
      // decir al resto que se ha unido
      socket.broadcast.emit('user joined', {
        username: socket.username,
        numUsers: numUsers
      })
    });

    socket.on('new message', function(data) {
      socket.broadcast.emit('new message', {
        username: socket.username,
        message: data
      });
    });

    socket.on('disconnect', function() {
      if (!addedUser) {
        return;
      }

      numUsers--;
      socket.broadcast.emit('user left', {
        username: socket.username,
        numUsers: numUsers
      })

    });


  });

}

module.exports = chatServer;