"use strict";

document.addEventListener('DOMContentLoaded', function() {

  var loggedIn = false;
  var socket = io();
  var inputMessage = document.querySelector('.inputMessage');
  var inputUsername = document.querySelector('.usernameInput');
  var ulMessages = document.querySelector('.messages');

  inputUsername.value = 1000 + Math.round(Math.random() * 9000);

  inputUsername.focus();
  inputUsername.select();

  // eventos

  // user enter name
  inputUsername.addEventListener('keydown', function(e) {
    if (e.keyCode === 13) {
      socket.emit('add user', inputUsername.value);
      inputMessage.focus();
    }
  });

  inputMessage.addEventListener('keydown', function(e) {
    if (e.keyCode === 13) {
      // mandar mensaje al servidor
      if (loggedIn) {
        socket.emit('new message', inputMessage.value);
        addChatMessage({username: '', message: inputMessage.value});
        inputMessage.value = '';
      }
    }
  });

  function log(message) {
    ulMessages.innerHTML += '<li class="text-muted">' + message + '</li>';
  }

  function addChatMessage(data) {
    var userName = data.username ? '<span class="tag tag-primary mr-1"/>' + data.username + '</span>'
      : '';
    var messageBody = '<span class="messageBody">' + data.message + '</span>';

    ulMessages.innerHTML += '<li class="message">' + userName + messageBody + '</li>';

  }

  socket.on('login', function(data) {
    loggedIn = true;
    log('Bienvenido al chat');
    log('Numero de usuarios: ' + data.numUsers);
  });

  socket.on('user joined', function(data) {
    log(data.username + ' se ha unido');
    log('Numero de usuarios: ' + data.numUsers);
  });

  socket.on('disconnect', function() {
    log('te has desconectado, verifica tu conexión a internet');
  });

  socket.on('reconnect', function() {
    log('te has reconectado');
    socket.emit('add user', inputUsername.value);
  });

  socket.on('new message', addChatMessage)

});