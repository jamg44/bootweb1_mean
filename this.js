"use strict";

function Coche() {
  this.ruedas = 4;
  this.cuantasRuedas = function() {
    console.log('tiene', this.ruedas);
    //console.log(this);
  }
}

const coche = new Coche();

// coche.cuantasRuedas();

const cuantasRuedas = coche.cuantasRuedas;
//cuantasRuedas.bind(coche)();

//setTimeout(coche.cuantasRuedas.bind(coche), 1000);

// cogemos metodo prestado para otro objeto

const camion = {
  ruedas: 18,
  //cuantasRuedas: coche.cuantasRuedas
};

//camion.cuantasRuedas();

//coche.cuantasRuedas.call(camion);

const cuantasRuedasCamion = coche.cuantasRuedas.bind(camion);

cuantasRuedasCamion();

