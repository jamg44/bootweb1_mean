"use strict";

function conArroz( plato) {
  return new Promise(function(resolve, reject) {
    resolve(plato + ' arroz');
    //reject('error fatal');
  });
}

function conAjo( plato) {
  return new Promise(function(resolve, reject) {
    resolve(plato + ' ajo');
  });
}

function con( plato, ing) {
  return new Promise(function(resolve, reject) {
    resolve(plato + ' ' + ing);
    // fs.readFile('/adsad/adsa', 'utf-8', function(err, data) {
    //   if (err) { return reject(err);}
    //   resolve(data;)
    // })
  });
}

const paella = 'paella con';

conArroz(paella)
  .then( conAjo)
  // .then(function(plato) {
  //   return conAjo(plato);
  // })
  .then( function(plato) {
    return con(plato, 'pollo');
  })
  .then( function(plato) {
    console.log(plato);
  }).catch(function(err) {
    console.log('Hubo un error', err);
  });

