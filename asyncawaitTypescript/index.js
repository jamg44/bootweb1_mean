"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
function sleep(texto, milis) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve(texto);
            //reject('error');
        }, milis);
    });
}
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        let res = yield sleep('Smith', 2000);
        console.log(res);
        try {
            JSON.parse('skjdhfkasdhfkl');
            //throw new Error('adas');
            for (let i = 0; i < 5; i++) {
                let res = yield sleep('Smith-' + i, 2000);
                console.log(res);
            }
        }
        catch (ex) {
            console.log('cazo la excepción:', ex);
        }
        return 'sfasda';
    });
}
main().then(data => {
    console.log('data', data);
}).catch(err => {
    console.log('Hubo un error', err);
});
