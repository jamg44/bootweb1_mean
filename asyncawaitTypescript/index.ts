"use strict";

function sleep(texto, milis) {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve(texto);
            //reject('error');
        }, milis);
    });
}

async function main() {
    let res = await sleep('Smith', 2000);
    console.log(res);

    try {
        JSON.parse('skjdhfkasdhfkl');
        //throw new Error('adas');
        for (let i = 0; i < 5; i++) {
            let res = await sleep('Smith-' + i, 2000);
            console.log(res);
        }
    } catch(ex) {
        console.log('cazo la excepción:', ex);
    }
    return 'sfasda';
}


main().then(data => {
    console.log('data', data);
}).catch(err => {
    console.log('Hubo un error', err);
});