"use strict";

const fs = require('fs');
const path = require('path');

function versionModulo(moduleName, callBack) {
  const fichero = path.join('./node_modules', moduleName, 'package.json');

  fs.readFile( fichero, 'utf8', function (err, data) {
    if (err) {
      callBack(err);
      return;
    }

    // JSON.parse es síncrono, por tanto para controlar si falla
    // puedo usar try/catch
    try {
      const packageJson = JSON.parse(data);
      callBack(null, packageJson.version);

    } catch(ex) {
      callBack(ex);
      return;
    }


  });

}

versionModulo('chance', function(err, str) {
  if (err) {
    console.log('Hubo un error:', err);
    return;
  }
  console.log('La versión del módulo chance es ', str);
});