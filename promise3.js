"use strict";

var fs = require('fs');

// función que lee un fichero retorna promesa y callback
function leeFichero( nomfile, callBack) {
  return new Promise(function(resolve, reject){
    fs.readFile(nomfile, 'utf8', function (err, data) {
      if (err) {
        if (callBack) {
          callBack(err);
        }
        reject(err);
        return;
      }

      if (callBack) {
        callBack(null, data);
      }
      resolve(data);
    });
  });
}

var file = './_ficherotexto.txt';

//leeFichero(file, function(err, data) {
//    if (err) { return console.log('ERROR', err); }
//    console.log(data);
//});

// leeFichero con promesa
leeFichero(file).then( function(data) {
  console.log(data);
}).catch( function(err) {
  console.log('ERROR', err);
});